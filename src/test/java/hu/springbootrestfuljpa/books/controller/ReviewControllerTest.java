package hu.springbootrestfuljpa.books.controller;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import pruebas.BookRepository;
import pruebas.ReviewRepository;
import pruebas.controller.BookController;
import pruebas.controller.ReviewController;
import pruebas.model.Book;
import pruebas.model.Review;

public class ReviewControllerTest 
{

	private static int ID = 2;
	private static int RELEASE = 22;
	private static String AUTHOR = "Homero";
	private static String TITLE = "Odisea";
	private static List<Review> REVIEW_LIST = new ArrayList<>();
	
	private static final Book BOOK = new Book();
	
	private static final Review rw = new Review();
	
	private static final Optional<Book> OPTIONAL_BOOK = Optional.of(BOOK);
	private static final Optional<Book> OPTIONAL_BOOK_EMPTY = Optional.empty();
	
	@Mock
	private BookRepository bookRepository;
	
	@Mock
	private ReviewRepository reviewRepository;

	@InjectMocks
	ReviewController reviewController;
	
	@BeforeEach
	public void init()
	{
		MockitoAnnotations.openMocks(this);
		BOOK.setAuthor(AUTHOR);
		BOOK.setId(ID);
		BOOK.setRelease(RELEASE);
		BOOK.setReviews(REVIEW_LIST);
		BOOK.setTitle(TITLE);
		rw.setBook(BOOK);
		rw.setDescription(AUTHOR);
		rw.setId(ID);
	}
	
	
	@Test
	public void retrieveAllReviewsTest()
	{
		Mockito.when(bookRepository.findById(ID)).thenReturn(OPTIONAL_BOOK);
		reviewController.retrieveAllReviews(ID);
	}
	
	@Test
	public void retrieveAllReviewsOKTest()
	{
		Mockito.when(bookRepository.findById(ID)).thenReturn(OPTIONAL_BOOK);
		ResponseEntity<List<Review>> response = reviewController.retrieveAllReviews(ID);
	
		assertEquals(response.getBody(), REVIEW_LIST); 
	}
	
	@Test
	public void retrieveAllReviewsNotFoundTest()
	{
		Mockito.when(bookRepository.findById(ID)).thenReturn(OPTIONAL_BOOK_EMPTY);
		ResponseEntity<List<Review>> response = reviewController.retrieveAllReviews(ID);
	
		assertEquals(response.getStatusCode(), HttpStatus.NOT_FOUND); 
	}
	
	
 
	///////////////////////
	
	@Test
	public void createReviewTest()
	{
		Mockito.when(bookRepository.findById(ID)).thenReturn(OPTIONAL_BOOK);
		reviewController.createReview(ID, rw);
	}
	
	@Test
	public void createReviewNotFoundTest()
	{

		Mockito.when(bookRepository.existsById(ID)).thenReturn(true);
		Review rw2 = reviewRepository.save(rw); //
		ResponseEntity<Object> response = reviewController.createReview(ID,rw2);
	
		assertEquals(response.getStatusCode(), HttpStatus.NOT_FOUND);
	}
	
	@Test
	public void createReviewExistTest()
	{
		Mockito.when(bookRepository.findById(ID)).thenReturn(OPTIONAL_BOOK);
		ResponseEntity<Object> response = reviewController.createReview(ID,rw);
		assertEquals(response.getStatusCode(), HttpStatus.OK);
	}
}
