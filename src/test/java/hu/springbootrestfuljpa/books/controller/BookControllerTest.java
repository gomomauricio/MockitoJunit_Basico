package hu.springbootrestfuljpa.books.controller;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import pruebas.BookRepository;
import pruebas.controller.BookController;
import pruebas.model.Book;
import pruebas.model.Review;

public class BookControllerTest 
{
	private static int ID = 2;
	private static int RELEASE = 22;
	private static String AUTHOR = "Homero";
	private static String TITLE = "Odisea";
	private static List<Review> REVIEW_LIST = new ArrayList<>();
	
	private static final Book BOOK = new Book(); 
	
	private static final Optional<Book> OPTIONAL_BOOK = Optional.of(BOOK);
	private static final Optional<Book> OPTIONAL_BOOK_EMPTY = Optional.empty();
	
	@Mock
	private BookRepository bookRepository;

	@InjectMocks
	BookController bookController;
	
	@BeforeEach
	public void init()
	{
		MockitoAnnotations.openMocks(this);
		BOOK.setAuthor(AUTHOR);
		BOOK.setId(ID);
		BOOK.setRelease(RELEASE);
		BOOK.setReviews(REVIEW_LIST);
		BOOK.setTitle(TITLE);
//		final Book book = new Book(); //se pone aqui si es general para todos los test
//		Mockito.when(bookRepository.findAll()).thenReturn(Arrays.asList(book));
	}
	
	
	@Test
	public void retrieveAllBooksTest()
	{
		final Book book = new Book(); //se pone aqui si solo esta clase usa estos metodos
		Mockito.when(bookRepository.findAll()).thenReturn(Arrays.asList(book));
	
		final List<Book> response = bookController.retrieveAllBooks();
		assertNotNull(response); //que no traiga nulos
		assertFalse(response.isEmpty()); //que la lista no este vacia
		assertEquals(response.size(), 1); //comprobamos que tenga dimension mayor a 1
	}
	
	
	@Test
	public void retrieveBookTest()
	{
		Mockito.when(bookRepository.findById(ID)).thenReturn(OPTIONAL_BOOK);
		ResponseEntity<Book>  response = bookController.retrieveBook(ID);
		assertEquals(response.getBody().getAuthor(), AUTHOR);
		assertEquals(response.getBody().getTitle(), TITLE);
	}
	
	@Test
	public void retrieveBookNotFoundTest()
	{
		Mockito.when(bookRepository.findById(ID)).thenReturn(OPTIONAL_BOOK_EMPTY);
		ResponseEntity<Book>  response = bookController.retrieveBook(ID);
		assertEquals(response.getStatusCode(), HttpStatus.NOT_FOUND);
	}
	
	
	@Test
	public void createBookTest()
	{
		Mockito.when(bookRepository.existsById(BOOK.getId())).thenReturn(false);
		
		ResponseEntity<Object>  response = bookController.createBook(BOOK);
		
		assertEquals( response.getStatusCode(), HttpStatus.OK);
	}
	
	
	@Test
	public void createBookExistByIdTest()
	{
		Mockito.when(bookRepository.existsById(BOOK.getId())).thenReturn(true);
		
		ResponseEntity<Object>  response = bookController.createBook(BOOK);
		
		assertEquals( response.getStatusCode(), HttpStatus.CONFLICT);
	}
	
	
	@Test
	public void deleteBook()
	{
		Mockito.when(bookRepository.findById(ID)).thenReturn(OPTIONAL_BOOK);
		 bookController.deleteBook(ID);
		 
	}
	
	@Test
	public void deleteNotFoundBook()
	{
		Mockito.when(bookRepository.findById(ID)).thenReturn(OPTIONAL_BOOK_EMPTY);
		 bookController.deleteBook(ID);
		 
		 ResponseEntity<Object>  response = bookController.deleteBook(ID);
		 
		 assertEquals(response.getStatusCode(), HttpStatus.NOT_FOUND); 
	}
}
