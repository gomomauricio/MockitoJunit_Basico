package pruebas.calc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class Calculadora_1_Test 
{
	//Aqui creamos (simular) lo que va hacer la interfaz
	@Mock
	CalculadoraGoogle calcGoogle;
	
	
	//inyectamos la clase a testear con mockito
	@InjectMocks
	Calculadora_1 calc;
	
	
	//lo que debe iniciar antes del test
	@BeforeEach 
	public void init()
	{
		//ponemos siempre
//		MockitoAnnotations.initMocks(this);
		MockitoAnnotations.openMocks(this);
		
		//creamos la simulacion
		when(calcGoogle.sumar(5,5)).thenReturn(10);
	}

	
	@Test
	public void sumaNuestraTest()
	{
		assertEquals(10, calc.sumaNuestra(5, 5));
	}
	
	
}
