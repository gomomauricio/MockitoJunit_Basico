package pruebas.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class IndexControllerTest
{
	@Test
	public void welcomeTest()
	{
		IndexController indexController = new IndexController();
		
		String[] params = null;
		
		String resultActual = "El array esta vacio";
		
		String resultReal = indexController.welcome(params);
		
		assertEquals(resultActual, resultReal);
		
	}
	
	@Test
	public void arraySinDatosTest()
	{
		IndexController indexController = new IndexController();
		
		String[] params = new String[3];
		
		String resultActual = "param[0]= null\n" + "param[1]= null\n" + "param[2]= null\n" ;
		
		String resultReal = indexController.welcome(params);
		System.out.println(" 1resultReal= " + resultReal );
		
		assertEquals(resultActual, resultReal);
		
	}
	
	
	@Test
	public void arrayConDatosTest()
	{
		IndexController indexController = new IndexController();
		
		String[] params = new String[] {"java","desde","0"};
		
		String resultActual = "param[0]= java\n" + "param[1]= desde\n" + "param[2]= 0\n" ;
		
		String resultReal = indexController.welcome(params);
		System.out.println(" 2resultReal= " + resultReal );
		
		assertEquals(resultActual, resultReal);
		
	}

}
