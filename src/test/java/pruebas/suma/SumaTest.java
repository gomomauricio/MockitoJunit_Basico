package pruebas.suma;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

 

/**
 * para porbar esta aplicacion, click derecho y en run as en Junit test
 * 
 * @author Ing. Mauricio Gonzalez Mondragon
 *
 */

public class SumaTest 
{
    //Aqui llevamos acabo el test unitario
	
	Suma sum = new Suma();
	
	//Ejecutar antes del test
	@BeforeEach
	public void before()
	{
		System.out.println("Se ejecuta antes ");
	}
	
	@Test
	public void sumaTest()
	{
		System.out.println("clase suma test");
		int sumTest = sum.suma(1, 2);
		
		int resultEsperado = 3;
		
		//le pasamos el RESULTADO esperado y el actual
		assertEquals(resultEsperado, sumTest);
	}
	
	
	//Ejecutar despues del test
		@AfterEach
		public void after()
		{
			System.out.println("Se ejecuta despues del test unitario ");
		}
	
	
}
