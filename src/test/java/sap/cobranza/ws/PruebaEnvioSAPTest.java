package sap.cobranza.ws;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import pruebas.model.Book;
import pruebas.sap.cobranza.model.Request;
import pruebas.sap.cobranza.model.Response;
import pruebas.sap.cobranza.service.SigsService;
import pruebas.sap.cobranza.ws.ObtenerPolizaWS;
import pruebas.sap.cobranza.ws.PruebaEnvioSAP_1;

public class PruebaEnvioSAPTest 
{
	
	private static final Request REQ  = new Request();
	private static final Request REQERROR  = new Request();

	private static final Response REP  = new Response();
	private static final Response REPOK  = new Response();
	private static final Response REPERROR  = new Response();
	
	private static final String USER = "USUARIO";  
	private static final String PASS = "CONTRASENIA";  
	private static final String USER0 = null;  
	private static final String PASS0 = null;  
	private static final Integer USER_OK = 2;  
	private static final Integer USER_FAIL = -1;  
	
	@Mock
	private SigsService sigsService;

	@InjectMocks
	PruebaEnvioSAP_1 pruebaEnvioS;
	
	@BeforeEach
	public void init()
	{
		MockitoAnnotations.openMocks(this);
		REQ.setUsuario("algo");
		REQ.setContrasena("algo");
		
		REP.setEstatus(-1); 
		REP.setNumero_liquidacion(null); 
		REP.setMesaje("Credenciales no validas");
		
		REPOK.setEstatus(1); 
		REPOK.setNumero_liquidacion("1"); 
		REPOK.setMesaje("exito");
		
		REPERROR.setEstatus(-1);
		REPERROR.setNumero_liquidacion(null); 
		REPERROR.setMesaje("Error de comunicacion BD"); 
		
//		final Book book = new Book(); //se pone aqui si es general para todos los test
//		Mockito.when(bookRepository.findAll()).thenReturn(Arrays.asList(book));
	}
	
	@Test
	public void obtenerPolizaTest()
	{
		Mockito.when( sigsService.getPoliza(REQ)).thenReturn(REP);
		
		 pruebaEnvioS.obtenerPoliza(REQ);
	 
	}
	
	@Test
	public void obtenerPoliza1Test()
	{
		Mockito.when( sigsService.isUserOk(USER, PASS)).thenReturn(USER_OK);
		
		Response res = pruebaEnvioS.obtenerPoliza(REQ);
	 
		assertEquals(2, USER_OK);
//		assertEquals(res.getNumero_liquidacion(), REPOK.getNumero_liquidacion());
//		assertEquals(res.getMesaje(), REPOK.getMesaje());
//		assertFalse();
	}
	
	 
	@Test
	public void obtenerPoliza2Test() throws Exception 
	{
		Mockito.when(pruebaEnvioS.obtenerPoliza(REQERROR)).thenThrow( new RuntimeException() );
		
		Response res = pruebaEnvioS.obtenerPoliza(REQ);
	 
		assertEquals(2, USER_OK);
//		assertEquals(res.getNumero_liquidacion(), REPOK.getNumero_liquidacion());
//		assertEquals(res.getMesaje(), REPOK.getMesaje());
//		assertFalse();
	}
	
	 
	 @Rule
	  public final ExpectedException exception = ExpectedException.none();
	 
	 @Test
	  public void doStuffThrowsIndexOutOfBoundsException() {
		 Response res = new Response();

	    exception.expect(Exception.class);
	    res = pruebaEnvioS.obtenerPoliza(REQERROR);
	  }
	 
	 
	 @Test
	 public void someTest() {
		 
		 Mockito.when(pruebaEnvioS.obtenerPoliza(REQERROR)).thenThrow( new RuntimeException() );
			
			 pruebaEnvioS.obtenerPoliza(REQERROR);
			 }

	/***************
	 * 
	 * public void validateParameters(Integer param ) 
	 * {
    		if (param == null) 
    		{
        		throw new NullPointerException("Null parameters are not allowed");
    		}
		}
		
		@Test
@DisplayName("Test assert NullPointerException")
void testGeneralException(TestInfo testInfo) {
    final ExpectGeneralException generalEx = new ExpectGeneralException();

     NullPointerException exception = assertThrows(NullPointerException.class, () -> {
            generalEx.validateParameters(null);
        });
    assertEquals("Null parameters are not allowed", exception.getMessage());
}
	 * 
	 * 
	 * 
	 */
	
	
//	@Test
//	public void isAuthenticatedNoValidoTEST()
//	{
//		Mockito.when( sigsService.getPoliza(REQ)).thenReturn(REP);
//		
//		Response res  = obtenerPolizaWS.obtenerPoliza(REQ);
////		Response res  = new Response();
////		res.setEstatus(-1); 
////		res.setNumero_liquidacion(null); 
////		res.setMesaje("Credenciales no validas");
//		 
//		assertEquals(res.getEstatus(), REP.getEstatus());
//		assertEquals(res.getNumero_liquidacion(), REP.getNumero_liquidacion());
//		assertEquals(res.getMesaje(), REP.getMesaje());
//	}
//	
//	@Test
//	public void isAuthenticatedErrorTEST()
//	{
////		Mockito.when( sigsService.getPoliza(REQ)).thenReturn(REP);//.thenThrow( new Exception("abc msg") );
//		Mockito.when( sigsService.getPoliza(REQ)).thenReturn(REPERROR).thenThrow( new RuntimeException("Exception") );
//		
//		
//				
//				Response res  =   obtenerPolizaWS.obtenerPoliza(REQ);
//				res.setEstatus(-1);
//				res.setNumero_liquidacion(null); 
//				res.setMesaje("Error de comunicacion BD"); 
//		 
//		assertEquals(res.getEstatus(), REPERROR.getEstatus());
//		assertEquals(res.getNumero_liquidacion(), REPERROR.getNumero_liquidacion());
//		assertEquals(res.getMesaje(), REPERROR.getMesaje());
//	}
//	
//	@Test
//	public void obtenerPolizaTest()
//	{
//		Mockito.when( sigsService.getPoliza(REQ)).thenReturn(REP);
//		
//		 obtenerPolizaWS.obtenerPoliza(REQ);
//	 
//	}
//	
//	@Test
//	public void obtenerPolizaUserOkTest()
//	{
//		Mockito.when( sigsService.isUserOk(USER, PASS)).thenReturn(USER_OK);
//		obtenerPolizaWS.obtenerPoliza(REQ);
//		
//		assertEquals(2, USER_OK);
////		if( !user.isEmpty() && !pass.isEmpty() )
////	    {
////    		if ( sigsService.isUserOk(user, pass) >= 1 )
////		    		 res = true; 
//		
//		
//		
//	}
//	
//	
//	@Test
//	public void isAuthenticatedNoValido2TEST()
//	{
//		Mockito.when( sigsService.getPoliza(REQ)).thenThrow();
//		
//		Response res  = obtenerPolizaWS.obtenerPoliza(REQ);
//		System.out.println("-->" + res.getMesaje());
////		Response res  = new Response();
////		res.setEstatus(-1); 
////		res.setNumero_liquidacion(null); 
////		res.setMesaje("Credenciales no validas");
//		 
//		assertEquals(res.getEstatus(), REP.getEstatus());
//		assertEquals(res.getNumero_liquidacion(), REP.getNumero_liquidacion());
//		assertEquals(res.getMesaje(), REP.getMesaje());
//	}
//
//	
	
	
}
