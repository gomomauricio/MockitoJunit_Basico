package com.boot.bookingrestaurantapi.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import pruebas.bookingrestaurantapi.controllers.RestaurantController;
import pruebas.bookingrestaurantapi.exceptions.BookingException;
import pruebas.bookingrestaurantapi.jsons.RestaurantRest;
import pruebas.bookingrestaurantapi.jsons.TurnRest;
import pruebas.bookingrestaurantapi.responses.BookingResponse;
import pruebas.bookingrestaurantapi.services.RestaurantService;

public class RestauranteControllerTest 
{
	private static final Long RESTAURANT_ID = 1L;
	private static final String SUCCES_STATUS = "Succes";
	private static final String SUCCES_CODE = "200 OK";
	private static final String OK = "OK";
	
	private static final RestaurantRest RESTAURANT_REST = new RestaurantRest();
	private static final String NAME = "Burger";
	private static final String DESCRIPTION = "Todo tipo de hamburgesas";
	private static final String ADDRES = "Av. Galindo";
	private static final String IMAGE = "www.burgermgm.com.mx";
	private static final List<TurnRest> TURN_LIST= new ArrayList<>();
	private static final List<RestaurantRest> RESTAURANT_REST_LIST= new ArrayList<>();
	
	
	@Mock
	RestaurantService restaurantService;
	
	@InjectMocks
	RestaurantController restaurantController;

	@BeforeEach
	public void init() throws BookingException
	{
		MockitoAnnotations.openMocks(this);
		
		RESTAURANT_REST.setName(OK);
		RESTAURANT_REST.setAddress(ADDRES);
		RESTAURANT_REST.setDescription(DESCRIPTION);
		RESTAURANT_REST.setId(RESTAURANT_ID);
		RESTAURANT_REST.setImage(IMAGE);
		RESTAURANT_REST.setTurns(TURN_LIST);
	}
	
	@Test
	public void getRestaurantByIdTest() throws BookingException
	{
		Mockito.when( restaurantService.getRestaurantById(RESTAURANT_ID)).thenReturn(RESTAURANT_REST);
		final BookingResponse<RestaurantRest> response = restaurantController.getRestaurantById(RESTAURANT_ID);  // lo que entra
		
		//lo que sale
		assertEquals(response.getStatus(), SUCCES_STATUS);
		assertEquals(response.getCode(), SUCCES_CODE);
		assertEquals(response.getMessage(), OK );
		assertEquals(response.getData(), RESTAURANT_REST );
		
		
	}
	
	
	@Test
	public void getRestaurantsTest() throws BookingException
	{
		Mockito.when( restaurantService.getRestaurants()).thenReturn(RESTAURANT_REST_LIST);
		final BookingResponse<List<RestaurantRest>> response = restaurantController.getRestaurants();  // lo que entra
		
		
		//lo que sale
				assertEquals(response.getStatus(), SUCCES_STATUS);
				assertEquals(response.getCode(), SUCCES_CODE);
				assertEquals(response.getMessage(), OK );
				assertEquals(response.getData(), RESTAURANT_REST_LIST );
		
	}

	
	
}
