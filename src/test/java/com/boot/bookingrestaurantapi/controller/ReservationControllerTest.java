package com.boot.bookingrestaurantapi.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Date;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import pruebas.bookingrestaurantapi.controllers.ReservationController;
import pruebas.bookingrestaurantapi.entities.Restaurant;
import pruebas.bookingrestaurantapi.exceptions.BookingException;
import pruebas.bookingrestaurantapi.jsons.CreateReservationRest;
import pruebas.bookingrestaurantapi.responses.BookingResponse;
import pruebas.bookingrestaurantapi.services.ReservationService;

public class ReservationControllerTest {

	private static final String SUCCES_STATUS = "Succes";
	private static final String SUCCES_CODE = "200 OK";
	private static final String OK = "OK";
	
	private static final Long RESTAURANT_ID = 1L;
	private static final Date DATE = new Date();
	private static final Long PERSON = 1L;
	private static final Long TURN_ID = 1L;
	private static final String LOCATOR = "burger 2";
//	private static final Optional<Restaurant> OPTINAL_RESTAURANT = Optional.of(Restaurant);
	Restaurant RESTAURANT = new Restaurant();	
	
	CreateReservationRest CREATE_RESERVATION = new CreateReservationRest();
	
	
	
	 @Mock
	 ReservationService reservationService;
	
	 @InjectMocks
	 ReservationController reservationController;
	
	@BeforeEach
	public void init() throws BookingException
	{
		MockitoAnnotations.openMocks(this);
		CREATE_RESERVATION.setDate(DATE);
		CREATE_RESERVATION.setPerson(PERSON);
		CREATE_RESERVATION.setRestaurantId(RESTAURANT_ID);
		CREATE_RESERVATION.setTurnId(TURN_ID);
		
		Mockito.when( reservationService.createReservation(CREATE_RESERVATION)).thenReturn(LOCATOR);
	
	}
	
	
	
	@Test
	public void createReservationTest()  throws BookingException
	{
		BookingResponse<String>  response = reservationController.createReservation(CREATE_RESERVATION) ;
				
//		 assertEquals(response.getStatus(), SUCCES_STATUS);
//		assertEquals(response.getCode(), SUCCES_CODE);
//		assertEquals(response.getMessage(), OK );
//		assertEquals(response.getData(), LOCATOR );
	}
	
	
	
	
}
