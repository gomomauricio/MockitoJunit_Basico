package com.boot.bookingrestaurantapi.services;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import pruebas.bookingrestaurantapi.entities.Board;
import pruebas.bookingrestaurantapi.entities.Reservation;
import pruebas.bookingrestaurantapi.entities.Restaurant;
import pruebas.bookingrestaurantapi.entities.Turn;
import pruebas.bookingrestaurantapi.exceptions.BookingException;
import pruebas.bookingrestaurantapi.exceptions.NotFountException;
import pruebas.bookingrestaurantapi.jsons.RestaurantRest;
import pruebas.bookingrestaurantapi.repositories.RestaurantRepository;
import pruebas.bookingrestaurantapi.services.RestaurantService;
import pruebas.bookingrestaurantapi.services.impl.RestaurantServiceImpl;
import pruebas.sap.cobranza.model.Response;

public class RestaurantServiceTest 
{
	private static final Long RESTAURANT_ID = 1L;
	
	private static final Restaurant RESTAURANT = new Restaurant();
	private static final String NAME = "Burger";
	private static final String DESCRIPTION = "Todo tipo de hamburgesas";
	private static final String ADDRES = "Av. Galindo";
	private static final String IMAGE = "www.burgermgm.com.mx";
	private static final List<Turn> TURN_LIST= new ArrayList<>();
	private static final List<Restaurant> RESTAURANT_LIST= new ArrayList<>();
	private static final List<Board> BOARD_LIST= new ArrayList<>();
	private static final List<Reservation> RESERVATION_LIST= new ArrayList<>();
	
	
		 @Mock
		 RestaurantRepository restaurantRepository;
		
		@InjectMocks
		RestaurantServiceImpl restaurantService;

		@BeforeEach
		public void init() throws BookingException
		{
			MockitoAnnotations.openMocks(this);
			
			RESTAURANT.setName(NAME);
			RESTAURANT.setAddress(ADDRES);
			RESTAURANT.setDescription(DESCRIPTION);
			RESTAURANT.setId(RESTAURANT_ID);
			RESTAURANT.setImage(IMAGE);
			RESTAURANT.setTurns(TURN_LIST);
			RESTAURANT.setBoards(BOARD_LIST);
			RESTAURANT.setReservations(RESERVATION_LIST);
		
		}
		
		@Test
		public void getRestaurantByIdTest() throws BookingException
		{
			Mockito.when( restaurantRepository.findById(RESTAURANT_ID)).thenReturn(Optional.of(RESTAURANT));
			
			restaurantService.getRestaurantById(RESTAURANT_ID);
			
		}
		
		@Test
		public void getRestaurantsTest() throws BookingException
		{
			final Restaurant restaurant = new Restaurant();
			 
			
			Mockito.when( restaurantRepository.findAll()).thenReturn(Arrays.asList(restaurant));
			
			 List<RestaurantRest> res = restaurantService.getRestaurants();
			 
			 assertNotNull(res); // evalua que no venga nulo
			 assertFalse(res.isEmpty()); //evalua que novenga vacio
			 assertEquals(res.size(), 1);
			 
			
		}
		
		  
		 @org.junit.Test(expected = BookingException.class)
		  public void getRestaurantByIdERRORTest() throws BookingException {
			 Mockito.when( restaurantRepository.findById(RESTAURANT_ID)).thenReturn(Optional.empty());
			 restaurantService.getRestaurantById(RESTAURANT_ID);

		    
		    fail();
		  }
		
		 
	 
		

}
