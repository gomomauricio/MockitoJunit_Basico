package pruebas.sap.cobranza.ws;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pruebas.sap.cobranza.model.Request;
import pruebas.sap.cobranza.model.Response;
import pruebas.sap.cobranza.service.SigsService;

/****************************************************************************************************
*FECHA DE CREACIÓN: 12 de Marzo de 2020.
*DESCRIPCIÓN: Objeto para crear la firma del servicio web
*
*ULTIMA MODIFICACION FECHA:19/03/2020
*ULTIMA MODIFICACION DESCRIPCION:Se actualizan las entradas del webservices.
*
*@author Mauricio González Mondragón  V.1.0.1
******************************************************************************************************/

@WebService
@Component("ObtenerPolizaWS")
public class ObtenerPolizaWS 
{
	private static final Logger logger = Logger.getLogger(SigsService.class);
	
	@Autowired
	SigsService sigsService;
	
	@Resource
    WebServiceContext context;
	
  
	
	public Response obtenerPoliza(@WebParam Request req )
	{
		//////////////////
		Response res = new Response();
		
	 
//		System.out.println("Tamaño de registros " + req.size()); 
		
		if( isAuthenticated(req.getUsuario(), req.getContrasena()) )
		{ 
			try
			{
				 res = sigsService.getPoliza(req);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				logger.error("Ocurrio un error al procesar la solicitud:"  + res.toString());
				res.setEstatus(-1);
				res.setNumero_liquidacion(null); 
				res.setMesaje("Error de comunicacion BD"); 
			}
		}
		else
		{ 
			res.setEstatus(-1); 
			res.setNumero_liquidacion(null); 
			res.setMesaje("Credenciales no validas");
			logger.info("Usuario No valido "  + res.toString());
		}
	  
		return res;
	}
	 
	
	private boolean isAuthenticated(String user, String pass) 
	{ 
	    Boolean res = false; 
	    
	    try
	    { 
	    	if( !user.isEmpty() && !pass.isEmpty() )
		    {
	    		if ( sigsService.isUserOk(user, pass) >= 1 )
			    		 res = true; 
				else
				 		 res = false;  
		    }
	    }
	    catch (Exception e) 
	    {
			res = false; 
			 logger.error("Error al buscar login " , e);
			e.printStackTrace();
		}
	    

	   return res;
	}
 
}
