package pruebas.sap.cobranza.ws;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;

import javax.jws.WebService;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.ws.WebServiceContext; 

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.stereotype.Component;

import pruebas.sap.cobranza.model.Request;
import pruebas.sap.cobranza.model.Response;
import pruebas.sap.cobranza.service.SigsService; 

/****************************************************************************************************
*FECHA DE CREACIÓN: 12 de Marzo de 2020.
*DESCRIPCIÓN: Objeto para crear la firma del servicio web
*
*MODIFICACION FECHA:19/03/2020
*MODIFICACION DESCRIPCION:Se actualizan las entradas del webservices.
*
**ULTIMA MODIFICACION FECHA:23/04/2020
*ULTIMA MODIFICACION DESCRIPCION:cambio de nombra para funcion de consumo.
*
*@author Mauricio González Mondragón  V.1.1.3
******************************************************************************************************/

@WebService
@Component("EstatusLiquidacionWS")
public class EstatusLiquidacionWS 
{
	private static final Logger logger = Logger.getLogger(SigsService.class);
	
	@Autowired
	SigsService sigsService;
	
	@Resource
    WebServiceContext context;
	 
	@WebMethod
	@WebResult(name = "return") 
//	@WebResult(name = "CXC") 
//	public Response liquidacionIngresoGS(@WebParam(name = "WS_CXC") @XmlElement(required = true) Request req )
	public Response liquidacionIngresoGS(@WebParam(name = "arg0") @XmlElement(required = true) Request req )
	{
		//////////////////
		Response res = new Response();
		
	 System.out.println("Usuario " + req.getUsuario() + "  Contrasenia:" + req.getContrasena());
//		System.out.println("Tamaño de registros " + req.size()); 
		
		if( isAuthenticated(req.getUsuario(),req.getContrasena()) )
		{ 
			try
			{
				res = prepararEnvio(req); 
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				logger.error("Ocurrio un error al procesar la solicitud:"  + req.toString());
				res.setEstatus(-1);
				res.setNumero_liquidacion(null); 
				res.setMesaje("Error de comunicacion BD"); 
			}
		}
		else
		{ 
			res.setEstatus(-1); 
			res.setNumero_liquidacion(null); 
			res.setMesaje("Credenciales no validas");
			logger.info("Usuario No valido "  + req.getUsuario() + "-" + req.getContrasena() );
		}
	  
		return res;
	}
	 
	
	private boolean isAuthenticated(String user, String pass) 
	{ 
	    Boolean res = false; 
	    
	    try
	    { 
	    	if( !user.isEmpty() && !pass.isEmpty() )
		    {
	    		res = sigsService.isUserOk(user, pass) >= 1 ? true : false; 
		    }
	    }
	    catch (Exception e) 
	    {
			res = false; 
			 logger.error("Error al buscar login " , e);
			e.printStackTrace();
		}
	    

	   return res;
	}
 
	
	private Response prepararEnvio(Request req)
	{
		Response res = new Response();
	
		if ( validarRequest(req ) )	
		{
				 res = sigsService.getPoliza(req);
		}
		else
		{
			res.setEstatus(-1); 
			res.setNumero_liquidacion(null); 
			res.setMesaje("Variables con caracteres no validos o variable vacia");
			logger.info("Variables con caracteres no validos o variable vacia"  + req.toString());
		}
		 
		 
		 return res;
	}
	
	private Boolean validarRequest(Request req)
	{
		Boolean res = true;  
		 
		if((req.getClaveBanco() == null)|| req.getClaveBanco().equals(0)) res = false; 
		if(req.getReferenciaCobro().isEmpty() || req.getReferenciaCobro().equals(null)) res = false; 
		if((req.getMonto() == null) || req.getMonto().equals(0.0) ) res = false; 
		if(req.getMoneda().isEmpty() || req.getMoneda().equals("?")  ) res = false; 
		if(req.getFechaCobroB().equals("?") ) res = false; 
		if(req.getHoraCobroB().equals("?")) res = false; 
		if(req.getPolizaContable().equals("?")) res = false; 
		 
		 return res;
	}
}
