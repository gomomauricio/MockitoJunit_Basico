package pruebas.sap.cobranza.ws;

import javax.annotation.Resource;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pruebas.sap.cobranza.model.Request;
import pruebas.sap.cobranza.model.Response;
import pruebas.sap.cobranza.service.SigsService;


@WebService
@Component("ObtenerPolizaWS")
public class PruebaEnvioSAP_1 
{
	private static final Logger logger = Logger.getLogger(SigsService.class);
	
	@Autowired
	SigsService sigsService;
	
	@Resource
    WebServiceContext context;
	
  
	
	public Response obtenerPoliza(@WebParam Request req )
	{
		//////////////////
		Response res = new Response();
		
			try
			{
				 res = sigsService.getPoliza(req);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				logger.error("Ocurrio un error al procesar la solicitud:"  + res.toString());
				res.setEstatus(-1);
				res.setNumero_liquidacion(null); 
				res.setMesaje("Error de comunicacion BD"); 
			}
	  
		return res;
	}
		
	
 
	 
 
 
	
}
