package pruebas.sap.cobranza.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional; 
import pruebas.sap.cobranza.dao.ObtenerPolizaDAO; 
import pruebas.sap.cobranza.dao.UtilDAO;
import pruebas.sap.cobranza.model.Request; 
import pruebas.sap.cobranza.model.Response; 
 

/**************************************************************************************************** 
*@author Daniel Uriel Rodriguez Ruiz 
******************************************************************************************************/


@Service
public class SigsService {
	private static final Logger logger = Logger.getLogger(SigsService.class);

	@Autowired
	ObtenerPolizaDAO obtenerPoliza; 
	
	@Autowired
	UtilDAO utilDAO;
	
//	@Transactional (value="sigsTxManager", readOnly = true)
	public Response getPoliza(Request re) 
	{ 
		return obtenerPoliza.getPoliza(re);
	} 
	
//	@Transactional (value="sigsTxManager", readOnly = true)
	public Integer isUserOk(String user,String pass) 
	{
		return utilDAO.isUsuarioValido(user, pass);	
	}
	
	 
	 
	 
	 
}
