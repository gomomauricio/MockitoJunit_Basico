package pruebas.sap.cobranza.dao;


import java.util.Iterator;
import org.apache.log4j.Logger;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import pruebas.sap.cobranza.model.PolizaRespuesta;
import pruebas.sap.cobranza.model.Request;
import pruebas.sap.cobranza.model.Response;
import pruebas.sap.cobranza.service.SigsService; 

/****************************************************************************************************
*FECHA DE CREACIÓN: 12 de Marzo de 2020.
*DESCRIPCIÓN: Objeto para orquestar el servicio web con el SP:sp_obtienepoliza;
*
*MODIFICACION FECHA:02/04/2020
*DESCRIPCION:Se actualizan el nombre del SP.
*MODIFICACION FECHA:13/04/2020
*MODIFICACION DESCRIPCION:Se actualizan la respuesta del SP.
*ULTIMA MODIFICACION FECHA:21/05/2020
*ULTIMA MODIFICACION DESCRIPCION:Se actualizan los parametros de entrada de sp.
*
*@author Mauricio González Mondragón  V.2.0.1
******************************************************************************************************/

@Repository
public class ObtenerPolizaDAO extends SigsGenericDAO 
{
	private static final Logger logger = Logger.getLogger(SigsService.class);
	
	/**
	 * {@code llamado al sp_cxc_referenciada }
	 * @param re Request modelo de entrada de datos
	 * @return response modelo de respuesta
	 */
	public Response getPoliza(Request re) 
	{
		Response response = new Response();  // respuesta del servicio web
		PolizaRespuesta pr = new PolizaRespuesta(); // respuesta de SP de la Base de datos.
		String nLiquidacion = ""; // numero_liquidacion; compuesto por sucursal - servicio - numeroLiq ( sucursal-serie-liquid)
		
		Integer res = 0;
		
		Query query = null; 
		
//		if(re.getFechaCobroB().isEmpty()) re.setFechaCobroB("null"); else re.setFechaCobroB("'" + re.getFechaCobroB() + "'");
		
		re.setFechaCobroB( (re.getFechaCobroB().isEmpty() ? "null" : "'" + re.getFechaCobroB() + "'")  );
		re.setHoraCobroB( (re.getHoraCobroB().isEmpty() ? "null" : "'" + re.getHoraCobroB() + "'")  );
		re.setPolizaContable( (re.getPolizaContable().isEmpty() ? "null" :  re.getPolizaContable() )  );
		
		try
		{ 
			String consulta = "Call sp_cxc_referenciada (" +
					            re.getClaveBanco() + ",'" + 
				                re.getReferenciaCobro() + "'," + 
					            re.getMonto()  + ",'" +
					            re.getMoneda() + "'," +  
					            re.getFechaCobroB() + "," +
					            re.getHoraCobroB() + ",'" +
					            re.getPolizaContable() + "');" ; 
		
			/*
			 * sp_cxc_referenciada(pbanco      smallint,
                                                preferencia varchar(60),
                                                pimporte    decimal(17,2),
                                                pmoneda     char(3),
                                                pfeccobro   char(10),
                                                phoracobro  char(10),
                                                ppoliza     integer
                                                )
			 */
			
			logger.info("Consulta: " + consulta);
 
		
			Query query2 = getSessionFactory().getCurrentSession()
					                          .createSQLQuery( consulta );
				 
			String mensajeError = "No definido";
			Integer codError = 99;
 
				@SuppressWarnings("unchecked")
				Iterator<Object> i = query2.list().iterator();
				
				while (i.hasNext()) 
				{
					Object[] row = (Object[]) i.next();
					
					 		try
							{
								if(row[0] != null) codError = (Integer)row[0];
								if(row[2] != null)	mensajeError = row[2].toString();
							}catch(Exception e) {}
					
					if(row[15] != null) pr.setEstatus( ((Short)row[15]).intValue() );
					if(row[16] != null) pr.setSucliq(  ((Short)row[16]).intValue() );
					if(row[17] != null) pr.setSerliq( ((Short)row[17]).intValue() );
					if(row[18] != null) pr.setNumliq( ((Short)row[18]).intValue() );
					System.out.println("row[15] " + row[15].toString());
//					response.setEstatus( ((Short)row[15]).intValue() );
//					response.setSucursalLiq( ((Short)row[16]).intValue() );
//					response.setServicioLiq( ((Short)row[17]).intValue() );
//					response.setNumeroLiq( ((Short)row[18]).intValue() );
					
					
		      }
				
//				// numero_liquidacion; compuesto por sucursal - servicio - numeroLiq ( sucursal-serie-liquid)
//				nLiquidacion = pr.getSucliq() + "-" + pr.getSerliq() + "-" + pr.getNumliq() ; 
				
				response.setEstatus(pr.getEstatus()); 
				response.setNumero_liquidacion(nLiquidacion); 
				
				res = -1;
				if( response.getEstatus() >= 0) res = response.getEstatus();
//				res = response.getEstatus();
				
				//Estatus de la Liquidacion 
				if(res == -1)
				{
					response.setEstatus(-1);
					response.setMesaje("Error de comunicacion soap xml caracteres no validos"); 
					logger.error("al obtener status >" + response.getEstatus() + "<" ); 
				}				
				if(res == 0)
				{
						response.setMesaje("Liquidacion Aplicada Correctamente");
						// numero_liquidacion; compuesto por sucursal - servicio - numeroLiq ( sucursal-serie-liquid)
						nLiquidacion = pr.getSucliq() + "-" + pr.getSerliq() + "-" + pr.getNumliq() ; 
						response.setNumero_liquidacion(nLiquidacion);
				}
				if(res == 1)
						response.setMesaje("Recibo sin pendiente de pago");
				if(res == 2)	
						response.setMesaje("No existe, Recibo");
				if(res == 3)	
						response.setMesaje("Existen recibos anteriores pendientes");
				if(res == 4)	
						response.setMesaje("Recibo ya existente en otra liquidacion"); 
				if(res == 5)	
					response.setMesaje("IMPORTE Diferente a Recibo de Pago");
				if(res == 6)	
					response.setMesaje("Fecha Limite de PAGO Extemporanea o NO VALIDA");
				if(res>=7)
				{
					response.setEstatus(codError);
					response.setMesaje(mensajeError);
				}
			
		}
		catch (Exception e) 
		{
			response.setEstatus(-1);
			response.setMesaje("Error de comunicacion " + response.getMesaje()); 
			logger.error("al consultar SP sp_cxc_referenciada " + e);
			e.printStackTrace();
		}
		
		    logger.info("Temina SP sp_cxc_referenciada " + response.getEstatus() + "-" + response.getMesaje() );
			return response;
					
		}
	
	 



}
