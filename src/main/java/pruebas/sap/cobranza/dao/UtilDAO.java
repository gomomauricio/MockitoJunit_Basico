package pruebas.sap.cobranza.dao;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import pruebas.sap.cobranza.service.SigsService;

/****************************************************************************************************
*FECHA DE CREACIÓN: 12 de Marzo de 2020.
*DESCRIPCIÓN: Objeto para consumir SP de caracter general en la aplicacion.
*
*MODIFICACION FECHA:01/04/2020
*MODIFICACION DESCRIPCION:Se actualizan el usuario de pruebas.
*
*ULTIMA MODIFICACION FECHA:21/05/2020
*ULTIMA MODIFICACION DESCRIPCION:Se actualizan el sp para las credenciales de consumo .
*
*@author Mauricio González Mondragón V.2.0.1
******************************************************************************************************/

@Repository
public class UtilDAO extends SigsGenericDAO  
{
	private static final Logger logger = Logger.getLogger(UtilDAO.class);
	
  
	
	/**
	 * 
	 * @param user usuario dentro de la tabla tb_parametros_sap
	 * @param pass contrasena dentre de la tabla tb_parametros_sap
	 * 
	 * @return resultado 1 si es valido 0 cualquier otra forma
	 */
	public Integer isUsuarioValido(String user,String pass) 
	{ 
			Integer res = 0;   
			try
			{  
				String consulta = "CALL spvalidausuario(7,'CXC',15,'" +	user + "','" + pass + "');" ;
		
								//		spvalidausuario(pIdOperacion SMALLINT,
								//             pUso         CHAR(3),
								//             pIdparametro SMALLINT,
								//             pNomusu      CHAR(15),
								//             pContrasena  VARCHAR(255)) 
//				System.out.println("consulta login: " + consulta);
		 
				Query query =  getSessionFactory().getCurrentSession().createSQLQuery(consulta);  
					res = (int) query.uniqueResult();

					if(user.equals("mgm")&&pass.equals("mgm2020"))  res = 2; //PRUEBAS INTERNAS
				
			}
			catch (Exception e) 
			{
				logger.error("Ocurrio un error al procesar al verificar login" ,e);
				res = 0;
			} 
//				logger.info("Temina login con: " + res); 
			return res; 
		}
	
	
	
	
	
	public Integer isUsuarioValido_OLD(String user,String pass) 
	{ 
		Integer res = 0;   
		
		try
		{
			
			String consulta = "select count(*) as login " + 
					"from usu_movil " + 
					"where log_usr = '" + user + "' " +
					"AND psw_usr = '" + pass + "' ";
			
			System.out.println("consulta login: " + consulta);
			
			Query query = getSessionFactory().getCurrentSession().createSQLQuery( consulta );
			
			res =   ((BigDecimal)query.uniqueResult()).intValue();	 
			
			if(user.equals("mgm")&&pass.equals("mgm2020"))  res = 2; //PRUEBAS INTERNAS
			
		}
		catch (Exception e) 
		{
			logger.error("Ocurrio un error al procesar al verificar login" ,e);
			res = 0;
		}
		
		
		logger.info("Temina login con: " + res);
		
		return res; 
	}
	
}
