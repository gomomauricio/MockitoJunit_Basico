package pruebas.sap.cobranza.model;

/****************************************************************************************************
*FECHA DE CREACIÓN: 12 de Marzo de 2020.
*DESCRIPCIÓN: Objeto para las variables de respuesta del web services.
*
*ULTIMA MODIFICACION FECHA:19/03/2020
*ULTIMA MODIFICACION DESCRIPCION:Se actualizan los titulos de las variables.
*
*@author Mauricio González Mondragón V.1.0.1
******************************************************************************************************/

public class Response 
{ 
	private Integer estatus;  	//Estatus de la Liquidacion  ,smallint
								//- -1  Errores ce comunicacion a base de datos y de JAVA en general
								//-  0  Liquidacion Aplicada Correctamente
								//-  1  Recibo no pendiente de pago
								//-  2  Recibo no existe
								//-  3  Existan recibos anteriores pendientes
								//-  4  Recibo ya exista en otra liquidación
	private String mensaje;  //Descripcion de el estatus
	
	private String numero_liquidacion; // compuesto por sucursal - servicio - numeroLiq ( sucursal-serie-liquid)

//	private Integer  sucursalliq;     //sucursal liquidacion, smallint
//	private Integer  servicioliq;     //servicio liquidacion, smallint
//	private Integer  numeroliq;     //numero liquidacion, smallint
	
	public Integer getEstatus() {
		return estatus;
	}

	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	public String getMesaje() {
		return mensaje;
	}

	public void setMesaje(String mesaje) {
		this.mensaje = mesaje;
	}

	public String getNumero_liquidacion() {
		return numero_liquidacion;
	}

	public void setNumero_liquidacion(String numero_liquidacion) {
		this.numero_liquidacion = numero_liquidacion;
	}

	@Override
	public String toString() {
		return "Response[ estatus:" + estatus + ", msj:" + mensaje + ", liquidacion:" + numero_liquidacion
				+ "]";
	}

	
	
	
	 
	 
	
	 
	
	 
      
}
