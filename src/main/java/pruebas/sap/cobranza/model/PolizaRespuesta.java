package pruebas.sap.cobranza.model;

/****************************************************************************************************
*FECHA DE CREACIÓN: 12 de Marzo de 2020.
*DESCRIPCIÓN: Objeto para el consumo de store procedure sp_obtienepoliza 
*
*@author Mauricio González Mondragón V.1.0.1
******************************************************************************************************/

public class PolizaRespuesta 
{

	private Integer sqlError;
	private Integer isamError;
	private String  errorInformacion;
	 
	private Integer banco   ;  // Banco ; smallint 
	private Integer num_suc ;  // Sucursal; smallint
	private Integer num_ram ;  // Ramo; smallint
	private Integer num_pol ;  // Poliza
	private String  tip_end ;  // Tipo Endoso
	private Integer num_end ;  // Numero Endoso
	private Integer num_rec ;  // Numero de Recibo
	private String  fec_lim ;  // Fecha Lim. Pago
	private String  num_fol ;  // FoliodelaPoliza
	private Double  importe ;  // Importe
	private String  fecha_sistema; //dd/mm/aaaa
	private String  tiempo_sistema;//hh:mm:ss
	                
	private Integer estatus; // smallint
				//Estatus de la Liquidacion 
				//- 0  Liquidacion Aplicada Correctamente
				//- 1  Recibo no pendiente de pago
				//- 2  Recibo no existe
				//- 3  Existan recibos anteriores pendientes
				//- 4  Recibo ya exista en otra liquidación
	private Integer  sucliq;     //smallint 
	private Integer  serliq;     // smallint
	private Integer  numliq;     // smallint
	public Integer getSqlError() {
		return sqlError;
	}
	public Integer getIsamError() {
		return isamError;
	}
	public String getErrorInformacion() {
		return errorInformacion;
	}
	public Integer getBanco() {
		return banco;
	}
	public Integer getNum_suc() {
		return num_suc;
	}
	public Integer getNum_ram() {
		return num_ram;
	}
	public Integer getNum_pol() {
		return num_pol;
	}
	public String getTip_end() {
		return tip_end;
	}
	public Integer getNum_end() {
		return num_end;
	}
	public Integer getNum_rec() {
		return num_rec;
	}
	public String getFec_lim() {
		return fec_lim;
	}
	public String getNum_fol() {
		return num_fol;
	}
	public Double getImporte() {
		return importe;
	}
	public String getFecha_sistema() {
		return fecha_sistema;
	}
	public String getTiempo_sistema() {
		return tiempo_sistema;
	}
	public Integer getEstatus() {
		return estatus;
	}
	public Integer getSucliq() {
		return sucliq;
	}
	public Integer getSerliq() {
		return serliq;
	}
	public Integer getNumliq() {
		return numliq;
	}
	public void setSqlError(Integer sqlError) {
		this.sqlError = sqlError;
	}
	public void setIsamError(Integer isamError) {
		this.isamError = isamError;
	}
	public void setErrorInformacion(String errorInformacion) {
		this.errorInformacion = errorInformacion;
	}
	public void setBanco(Integer banco) {
		this.banco = banco;
	}
	public void setNum_suc(Integer num_suc) {
		this.num_suc = num_suc;
	}
	public void setNum_ram(Integer num_ram) {
		this.num_ram = num_ram;
	}
	public void setNum_pol(Integer num_pol) {
		this.num_pol = num_pol;
	}
	public void setTip_end(String tip_end) {
		this.tip_end = tip_end;
	}
	public void setNum_end(Integer num_end) {
		this.num_end = num_end;
	}
	public void setNum_rec(Integer num_rec) {
		this.num_rec = num_rec;
	}
	public void setFec_lim(String fec_lim) {
		this.fec_lim = fec_lim;
	}
	public void setNum_fol(String num_fol) {
		this.num_fol = num_fol;
	}
	public void setImporte(Double importe) {
		this.importe = importe;
	}
	public void setFecha_sistema(String fecha_sistema) {
		this.fecha_sistema = fecha_sistema;
	}
	public void setTiempo_sistema(String tiempo_sistema) {
		this.tiempo_sistema = tiempo_sistema;
	}
	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}
	public void setSucliq(Integer sucliq) {
		this.sucliq = sucliq;
	}
	public void setSerliq(Integer serliq) {
		this.serliq = serliq;
	}
	public void setNumliq(Integer numliq) {
		this.numliq = numliq;
	}
	
	
	
	
	
	
}
