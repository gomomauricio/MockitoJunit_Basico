package pruebas.sap.cobranza.model;

import java.lang.annotation.Documented;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
 
/****************************************************************************************************
*FECHA DE CREACIÓN: 12 de Marzo de 2020.
*DESCRIPCIÓN: Objeto para las variables de entrada del web services.
*
*MODIFICACION FECHA:19/03/2020
*MODIFICACION DESCRIPCION:Se actualizan los titulos de las variables.
*
*ULTIMA MODIFICACION FECHA:21/05/2020
*ULTIMA MODIFICACION DESCRIPCION:Se agrega la variable moneda.
*
*@author Mauricio González Mondragón  V.2.0.1
******************************************************************************************************/

@XmlType
@XmlAccessorType(XmlAccessType.FIELD)    
@XmlRootElement
public class Request 
{    
	@XmlAttribute(name="usuario", required=true) 
	private String usuario;
	@XmlAttribute(name="contrasena", required=true) 
	private String contrasena;
 
	@XmlElement(name="claveBanco", required=true)  
	private Integer claveBanco;  		//Clave del banco
	
	@XmlElement(name="referenciaCobro", required=true)
	private String referenciaCobro;		//Referencia de cobro
	
	@XmlElement(name="monto", required=true)
	private Double monto;	//Monto
	
	@XmlElement(name="moneda", required=true)
	private String moneda;	//tres carateres
	
	@XmlElement(name="fechaCobroB", required=true)
	private String fechaCobroB;			//Fecha de cobro del banco dd/mm/aaaa
	
	@XmlElement(name="horaCobroB", required=true)
	private String horaCobroB;			//Hora de cobro del banco  hh:mm:ss
	
	@XmlElement(name="polizaContable", required=true)
	private String polizaContable;		//No. documento de SAP (póliza contable)
	
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getContrasena() {
		return contrasena;
	}
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	public Integer getClaveBanco() {
		return claveBanco;
	}
	public void setClaveBanco(Integer claveBanco) {
		this.claveBanco = claveBanco;
	}
	public String getReferenciaCobro() {
		return referenciaCobro;
	}
	public void setReferenciaCobro(String referenciaCobro) {
		this.referenciaCobro = referenciaCobro;
	}
	public Double getMonto() {
		return monto;
	}
	public void setMonto(Double monto) {
		this.monto = monto;
	}
	public String getFechaCobroB() {
		return fechaCobroB;
	}
	public void setFechaCobroB(String fechaCobroB) {
		this.fechaCobroB = fechaCobroB;
	}
	public String getHoraCobroB() {
		return horaCobroB;
	}
	public void setHoraCobroB(String horaCobroB) {
		this.horaCobroB = horaCobroB;
	}
	public String getPolizaContable() {
		return polizaContable;
	}
	public void setPolizaContable(String polizaContable) {
		this.polizaContable = polizaContable;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	@Override
	public String toString() {
		return " Req[banco:" + claveBanco + ",refCobro:" + referenciaCobro 
				+ ",monto:" + monto + ",moneda:" + moneda + ",fCobro:"
				+ fechaCobroB + ",hCobro:" + horaCobroB + ",poliza:" + polizaContable + "]";
	}
	
	 
	
	
	
	 
	 
	 
	
	 
	
	

}
