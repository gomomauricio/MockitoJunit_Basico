/**
 * 
 */
package pruebas.calc;

/**
 * @author Ing. Mauricio Gonzalez Mondragon
 *
 */
public interface CalculadoraGoogle 
{
	public int sumar(int... num);

}
