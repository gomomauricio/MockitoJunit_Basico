package pruebas.calc;

public class Calculadora_1 {
	
	CalculadoraGoogle calcGoogle;  // si la clase depende de otra se debe usar Mockito
	
	public int sumaNuestra(int a, int b)
	{
		return calcGoogle.sumar(a,b);
	}

}
