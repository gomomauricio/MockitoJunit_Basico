package pruebas.bookingrestaurantapi.services;

import pruebas.bookingrestaurantapi.exceptions.BookingException;
import pruebas.bookingrestaurantapi.jsons.CreateReservationRest;
import pruebas.bookingrestaurantapi.jsons.ReservationRest;

public interface ReservationService {
	
	ReservationRest getReservation(Long reservationId) throws BookingException;
	
	String createReservation(CreateReservationRest CreateReservationRest) throws BookingException;

}
