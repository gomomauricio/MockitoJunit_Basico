package pruebas.bookingrestaurantapi.services;

import pruebas.bookingrestaurantapi.exceptions.BookingException;

public interface CancelReservationService {
	
	public String deleteReservation(String locator) throws BookingException;

}
