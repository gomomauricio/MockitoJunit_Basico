package pruebas;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pruebas.model.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Integer> {
}