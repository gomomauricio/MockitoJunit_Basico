package pruebas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MockitoJunitBasicoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MockitoJunitBasicoApplication.class, args);
		System.out.println("Iniciando Mockito y JUnit");
	}

}
