package pruebas.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class IndexController {
	@PostMapping("/welcome")
	@ResponseBody
	public String welcome(@RequestParam(required = false, name = "param") String[] params) {
		String msg = "";
		int i = 0;

		if (params == null) {
			msg = "El array esta vacio";
		} else {
			for (String st : params) {
				msg += "param[" + i + "]= " + st + "\n";
				i++;
			}
		}

		return msg;
	}
}
