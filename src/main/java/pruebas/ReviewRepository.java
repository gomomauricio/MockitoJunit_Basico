package pruebas;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pruebas.model.Review;

@Repository
public interface ReviewRepository extends JpaRepository<Review, Integer> {
}
