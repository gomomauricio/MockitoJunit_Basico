# Código del curso Test unitario con junit y mockito en Spring Java 8

# Instructor
> [Cristian Rojas](https://www.udemy.com/course/test-unitario-con-junit-y-mockito-en-spring-boot-java-8/) 
 

#Udemy
* Test unitario con junit y mockito en Spring Java 8

## Contiene

* Tipos de pruebas en el desarrollo de software.
* Test unitario con Junit.
* Test unitario con Mockito.
* Crear test unitario en Controladores y Servicios.
 

## Descripción
    Junit, es el un framework que nos permite escribir y ejecutar pruebas unitarias en Java. Con estas pruebas, buscamos evaluar si la unidad de trabajo responde correctamente a los test.

    Mockito, es un framework de código abierto, que nos permite la creación de objetos simulados, con el propósito de realizar pruebas unitarias en Java.

    Diferencias entre el testing con JUnit y Mockito.

---

## Notas
 
~~~
* Repositorio solo en ** Gitlab  **

* Pruebas Funcionales -> test unitario, de integracion, aceptacion (prueban el codigo hasta cumplir el requisito del cliente).

* Pruebas No Funcionales -> test instalacion. seguridad, performance, de carga, de estres, de volumen(ya cuando el aplicativo esta terminado).

* * Pruebas unitarias -> comprueba el correcto funcionamiento del código generado.

    ------------------------ JUNIT ---------------------
* @Before ->Se ejecuta antes del metodo
* @After -> Se ejecuta despues del metodo
* @Test -> inyecta el bean de test al metodo
* @assertEquals -> compara resultados

* *El coverage nos puede decir si testeamos todo el codigo.

    ------------------------ Mockito  ---------------------
* Mockito es un framework para crear objetos simulados (moks) como ejemplo entrar a una base de datos.

* @Mock -> simula el servicio
* @InjectMock -> inyecta la clase a testear
* @Mockito.when -> Funciones de ayuda para mockear servicios.



/********************* Para Angular *********/
En la carpeta donde nos encontremos correr el servidor 
C:\directorio\MockitoJunit_Basico\angular-client-books>ng serve
con:
ng -serve

 ~~~


---
## Código 

`//desarrollo` 
 `public int suma( int x, int y ) { return x + y; }` 

 `//test` 
`@Test`
`public int suma( int x, int y ) {  assertEquals(2, ( x + y ) ); }`


 

---
#### Version  V.0.1 
* **V.1.0**  _>>>_  Iniciando proyecto [  on 10 SEP, 2021 ]  
 
 